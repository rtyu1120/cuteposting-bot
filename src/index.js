require('dotenv').config()

const TelegramBot = require('node-telegram-bot-api')
const options = process.env.ENV === 'dev' ? { polling: true } : { webHook: { port: process.env.PORT } }
const bot = new TelegramBot(process.env.BOT_TOKEN, options)

if (process.env.ENV === 'dev') bot.setWebHook(`${process.env.APP_URL}/bot${TOKEN}`)

const PixivAppApi = require('pixiv-app-api')
const pixiv = new PixivAppApi(process.env.PIXIV_ID, process.env.PIXIV_PASS)

getRemainingIllusts(73573157)
  .then(console.log)
  .catch(console.error)

/**
 * Get new pixiv illusts uploaded after last seen post (termnation id).
 * see https://github.com/akameco/pixiv-app-api for details.
 * 
 * @param {number} termnationId 
 * @returns {Promise<Array>} illusts
 */
async function getRemainingIllusts(termnationId) {
  let illusts = (await pixiv.illustFollow({ restrict: 'public' })).illusts
  const firstMatchedIndex = illusts.findIndex(i => i.id === termnationId)
  if (firstMatchedIndex !== -1) {
    illusts = illusts.slice(0, firstMatchedIndex)
    return illusts
  }
  while(pixiv.hasNext()) {
    let next = (await pixiv.next()).illusts
    let matchedIndex = next.findIndex(i => i.id === termnationId)
    if(matchedIndex !== -1) {
      illusts = illusts.concat(next.slice(0, matchedIndex))
      break;
    }
    illusts = illusts.concat(next)
  }
  return illusts;
}